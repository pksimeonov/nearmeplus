package com.example.pavel.nearme;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import java.io.InputStream;
import java.lang.ref.WeakReference;

/**
 * Created by Pavel on 12/12/2014.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    //Using weak reference for automatic removal once the image
    //is not referenced from outside anymore
    private final WeakReference imageViewReference;

    private static String log_tag = DownloadImageTask.class.getSimpleName();

    public DownloadImageTask(ImageView imageView) {
        imageViewReference = new WeakReference(imageView);
    }

    static Bitmap downloadImage(String image_url) {
        final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("getImage");
        final HttpGet httpGetRequest = new HttpGet(image_url);

        try { //try to connect using HTTP
            HttpResponse httpResponse = httpClient.execute(httpGetRequest);
            final int httpStatusCode = httpResponse.getStatusLine().getStatusCode();

            //if the connection is not successful - log status code and url
            if (httpStatusCode != HttpStatus.SC_OK) {
                Log.w(log_tag, "Error " + httpStatusCode
                        + " from " + image_url);
                return null;
            }

            final HttpEntity httpEntity = httpResponse.getEntity();

            if (httpEntity != null) {
                InputStream inputStream = null;

                try {
                    inputStream = httpEntity.getContent();
                    //Bitmap factory decodes the input stream into a bitmap image
                    return BitmapFactory.decodeStream(inputStream);
                }

                finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    httpEntity.consumeContent();
                }
            }
        }

        catch (Exception e) { //abort connection in case of exception error
            httpGetRequest.abort();
        }

        finally {
            if (httpClient != null) {
                httpClient.close();
            }
        }

        return null;
    }

    @Override
    //Using doInBackground to avoid lengthily processes on main thread
    //as this could cause UI lag/stutter
    protected Bitmap doInBackground(String... params) {
        return downloadImage(params[0]);
    }

    @Override //When the image is downloaded - assign it to its imageView
    protected void onPostExecute(Bitmap image) {
        if (isCancelled()) {
            image = null;
        }

        if (imageViewReference != null) {
            ImageView imageView = (ImageView) imageViewReference.get();

            if (imageView != null) {
                if (image != null) {
                    imageView.setImageBitmap(image);
                }
                else {
                    imageView.setImageDrawable(imageView.getContext()
                            .getResources().getDrawable(R.drawable.ic_launcher));
                }
            }
        }
    }

}
