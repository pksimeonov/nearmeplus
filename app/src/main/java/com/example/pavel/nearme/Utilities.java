package com.example.pavel.nearme;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Pavel on 12/12/2014.
 */
public class Utilities {

    private static String log_title = Utilities.class.getSimpleName();

    private static ConnectivityManager connectivity
            = (ConnectivityManager) MainActivity.getAppContext()
            .getSystemService(Context.CONNECTIVITY_SERVICE);

    //Check fo network connection
    public static Boolean checkInternetState() {
        NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    //Formats the place type to the API's standard
    //all lower case and underscore instead of space
    public static String formatPlaceType(String place) {
        place = place.replaceAll(" ", "_").toLowerCase();
        return place;
    }

    //Convert the whole string to lowercase
    //Then convert only the first letter of each word to uppercase.
    public static String toTitleCase(String givenString) {
        givenString = givenString.toLowerCase(); //convert string to lowercase
        String[] arr = givenString.split(" "); //split string at every space
        StringBuilder stringBuilder = new StringBuilder();
        for (String anArr : arr) {
            //convert first letter to uppercase
            stringBuilder.append(Character.toUpperCase(anArr.charAt(0)))
                    .append(anArr.substring(1)).append(" ");
        }
        return stringBuilder.toString().trim();
    }

    public static ArrayList<PlaceItem> extractJsonData(String placesJsonStr) throws JSONException {
        //The JSON contains the following objects:
        final String RESULTS = "results";
        final String NAME = "name";
        final String IMAGE_URL = "icon";
        final String OPEN_HOURS = "opening_hours";
        final String OPEN_NOW = "open_now";
        final String ADDRESS = "vicinity";
        final String NONE = "N/A";

        JSONObject placesJson = new JSONObject(placesJsonStr);
        JSONArray placesArray = placesJson.getJSONArray(RESULTS);

        String[] jsonResultArray = new String[placesArray.length()];
        ArrayList<PlaceItem> jsonPlacesArray = new ArrayList<>();

        for(int i = 0; i < placesArray.length(); i++) {
            PlaceItem place = new PlaceItem();

            //placeResult holds each place's details
            JSONObject placeResult = placesArray.getJSONObject(i);
            //Check if the JSON contains name key-value pair
            if (placeResult.has(NAME)) {
                //Get the name of the place, format it, and store it in the class
                place.setName(Utilities.toTitleCase(placeResult.getString(NAME)));
            }
            else {
                place.setName(NONE);
            }

            //Check if the JSON contains the open hours data, as not every venue/place does
            if (placeResult.has(OPEN_HOURS)) {
                //if it does - get the current status of the place and save it to the class
                if (placeResult.getJSONObject(OPEN_HOURS).getString(OPEN_NOW).equals("false"))
                    place.setOpen("Closed");
                else
                    place.setOpen("Open");
            }
            else { //if the JSON doesn't contain open hours data - set Not Available (N/A)
                place.setOpen(NONE);
            }

            if (placeResult.has(ADDRESS)) {
                place.setAddress(placeResult.getString(ADDRESS));
            }
            else {
                place.setAddress(NONE);
            }

            place.setImageURL(placeResult.getString(IMAGE_URL));

            jsonPlacesArray.add(place);

            jsonResultArray[i] = place.getName(); //For debugging purposes only
        }

        for (String str : jsonResultArray) //For debugging purposes only
            Log.v(log_title, "List entry" + str);

        return jsonPlacesArray;
    }
}
