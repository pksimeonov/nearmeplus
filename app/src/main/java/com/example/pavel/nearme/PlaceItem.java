package com.example.pavel.nearme;

/**
 * Created by Pavel on 12/12/2014.
 */
public class PlaceItem {

    private String name;
    private String open;
    private String address;
    private String imageURL;

    public PlaceItem() {
        this.name = "N/A";
        this.open = "N/A";
        this.imageURL = "N/A";
        this.address = "N/A";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
