package com.example.pavel.nearme;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static Context context;
    private static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivity.context = getApplicationContext();
        activity = this;

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    protected static Context getAppContext(){
        return MainActivity.context;
    }

    protected static Activity getAppActivity(){
        return MainActivity.activity;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            final String[] displayed_place_types = {
                    "Bank",
                    "Bar",
                    "Cafe",
                    "Convenience Store",
                    "Food",
                    "Liquor Store",
                    "Takeaway",
                    "Pharmacy",
                    "Post Office",
                    "Restaurant"
            };

            List<String> placeTypesList = new ArrayList<String>(Arrays.asList(displayed_place_types));

            ArrayAdapter<String> placesAdapter = new ArrayAdapter<String>(
                    getActivity(),
                    R.layout.list_item_place_types,
                    R.id.text_view_place_type,
                    placeTypesList
            );

            ListView listView = (ListView) rootView.findViewById(R.id.list_view_place_types);
            listView.setAdapter(placesAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //selectedPlaceType = displayed_place_types[position];
                    Intent newActivity = new Intent(view.getContext(), DisplayPlaces.class);
                    //Pass the variable and its value to the new activity
                    newActivity.putExtra("selectedPlaceType", displayed_place_types[position]);
                    startActivity(newActivity);
                }
            });

            return rootView;
        }
    }
}
