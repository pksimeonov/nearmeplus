package com.example.pavel.nearme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A custom adapter for the results list view
 */
public class CustomAdapter extends BaseAdapter {

    private ArrayList placesData;

    private LayoutInflater layoutInflater;

    static class ViewHolder {
        TextView textViewPlaceName;
        TextView textViewPlaceOpen;
        TextView textViewPlaceAddress;
        ImageView imageViewPlace;
    }

    public CustomAdapter(Context context, ArrayList placesData) {
        this.placesData = placesData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return placesData.size();
    }

    @Override
    public Object getItem(int position) {
        return placesData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.list_item_places, null);
            //Using viewHolder for more efficient code,
            //as using findView to search through the view tree is done only once
            viewHolder = new ViewHolder();
            viewHolder.textViewPlaceName = (TextView) convertView.findViewById(R.id.text_view_place_name);
            viewHolder.textViewPlaceOpen = (TextView) convertView.findViewById(R.id.text_view_place_open);
            viewHolder.textViewPlaceAddress = (TextView) convertView.findViewById(R.id.text_view_place_address);
            viewHolder.imageViewPlace = (ImageView) convertView.findViewById(R.id.image_view_place);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PlaceItem placeItem = (PlaceItem) placesData.get(position);

        viewHolder.textViewPlaceName.setText(placeItem.getName());
        viewHolder.textViewPlaceOpen.setText(placeItem.getOpen());
        viewHolder.textViewPlaceAddress.setText(placeItem.getAddress());

        if (viewHolder.imageViewPlace != null) {
            new DownloadImageTask(viewHolder.imageViewPlace).execute(placeItem.getImageURL());
        }

        return convertView;
    }

}
