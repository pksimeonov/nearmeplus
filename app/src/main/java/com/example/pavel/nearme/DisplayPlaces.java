package com.example.pavel.nearme;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class DisplayPlaces extends ActionBarActivity {

    private static String selectedValueType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_places);
        //Get the selected venue type
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            selectedValueType = extras.getString("selectedPlaceType", null);
        }
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_places, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String log_title = PlaceholderFragment.class.getSimpleName();

        private static LocationHandler locationHandler = new LocationHandler();

        public static void updatePlacesData() {
            getNearPlaces placesData = new getNearPlaces();

            Log.v(log_title, locationHandler.getLatitude()
                    + "," + locationHandler.getLongitude());

            placesData.execute(
                    locationHandler.getLatitude()
                    + "," + locationHandler.getLongitude(),
                    Utilities.formatPlaceType(selectedValueType) //format to the API standard
            );
        }

        public PlaceholderFragment() {
        }

        public static ListView listViewPlaces;

        @Override
        public void onStart() {
            super.onStart();
            //Start the Network status receiver
            NetworkStatusChangeReceiver networkChangeReceiver
                    = new NetworkStatusChangeReceiver();
            //If there is network connection
            if (Utilities.checkInternetState()) {
                updatePlacesData();
            }
            else {
                Toast.makeText(MainActivity.getAppContext(),
                        "Internet connection required!",
                        Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onPause() {
            locationHandler.stopListener();
            super.onPause();
        }

        @Override
        public void onResume() {
            super.onResume();
            locationHandler.registerListener();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_display_places, container, false);
            listViewPlaces = (ListView) rootView.findViewById(R.id.list_view_places);
            return rootView;
        }

        public static class getNearPlaces extends AsyncTask<String, Void, ArrayList<PlaceItem>> {

            ProgressDialog progressDialog;

            @Override
            protected ArrayList<PlaceItem> doInBackground(String... params) {

                //My application's API key provided by Google API services
                final String API_KEY = "AIzaSyDoQXOiKG8rVWBmsuLIwSgThkDloxXQqAQ";
                //Build API query URL
                final String api_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
                final String location_param = "location";
                final String rankby_param = "rankby";
                final String key_param = "key";
                final String place_type_param = "types";

                // Keep outside try method, so that they can be closed later
                HttpURLConnection urlConnection = null;
                BufferedReader bufferReader = null;

                // Will contain the raw JSON response as a string.
                String placesJsonStr = null;

                try {

                    Uri buildUri = Uri.parse(api_url).buildUpon()
                            .appendQueryParameter(location_param, params[0])
                            .appendQueryParameter(rankby_param, "distance")
                            .appendQueryParameter(place_type_param, params[1])
                            .appendQueryParameter(key_param, API_KEY).build();

                    URL url = new URL(buildUri.toString());

                    Log.v(log_title, "URI" + buildUri.toString());

                    // Create the request to Google Places API and open the connection
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.connect();

                    // Read the input stream into a String
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuilder stringBuilder = new StringBuilder();
                    if (inputStream == null) {
                        return null;
                    }
                    bufferReader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    while ((line = bufferReader.readLine()) != null) {
                        // Adding a newline won't affect parsing, but makes debugging easier to read
                        // Print out stringBuilder for debugging purposes
                        stringBuilder.append(line).append("\n");
                    }

                    if (stringBuilder.length() == 0) {
                        return null;
                    }
                    placesJsonStr = stringBuilder.toString();
                    Log.v(log_title, "JSON data" + placesJsonStr);

                } catch (IOException e) {
                    Log.e(log_title, "Error ", e);
                    return null;
                } finally{
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                    if (bufferReader != null) {
                        try {
                            bufferReader.close();
                        } catch (final IOException e) {
                            Log.e(log_title, "Error in closing buffer", e);
                        }
                    }
                }

                try {
                    return Utilities.extractJsonData(placesJsonStr);
                }
                catch (JSONException e) {
                    Log.e(log_title, e.getMessage(), e);
                    e.printStackTrace();
                }
                //return null if the data parsing didn't work
                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(MainActivity.getAppActivity());
                progressDialog.setMessage("Loading, please wait...");
                progressDialog.setIndeterminate(false);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(true);
                progressDialog.show();
            }

            @Override
            protected void onPostExecute(ArrayList<PlaceItem> jsonPlacesResult) {
                if (jsonPlacesResult != null) {
                    //When AsyncTask is finished - attach the custom adapter to the complex list view
                    listViewPlaces.setAdapter(new CustomAdapter(MainActivity.getAppActivity(), jsonPlacesResult));
                    progressDialog.dismiss();
                }
            }
        }
    }
}
