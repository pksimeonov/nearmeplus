package com.example.pavel.nearme;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Pavel on 12/12/2014.
 */
public class LocationHandler {

    private LocationManager locationManager;
    private LocationListener locationListener;

    private static Location location;

    public LocationHandler(){
        locationManager = (LocationManager) MainActivity.getAppContext().getSystemService(Context.LOCATION_SERVICE);

        setupListener();
        registerListener();
    }

    private void setupListener(){
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                LocationHandler.setLocation(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

    public Location getLocation() {
        try {
            Boolean isGPSEnabled, isNetworkEnabled ;
            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        0, //time interval for location update
                        0, //distance change for location update
                        locationListener);
                Log.d("Network", "Network Enabled");
                if (locationManager != null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            0,
                            0, locationListener);
                    Log.d("GPS", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    public void registerListener(){
        location = getLocation();
    }

    public void stopListener(){
        locationManager.removeUpdates(locationListener);
    }

    public static void setLocation(Location location) {
        LocationHandler.location = location;
    }

    public String getLatitude(){
        return Double.toString(location.getLatitude());
    }

    public String getLongitude(){
        return Double.toString(location.getLongitude());
    }

}
