package com.example.pavel.nearme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

/**
 * Broadcast receiver used to automatically query the API
 * once an internet connection is enabled again.
 */
public class NetworkStatusChangeReceiver extends BroadcastReceiver {

    public NetworkStatusChangeReceiver() {
        IntentFilter networkStateFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        MainActivity.getAppContext().registerReceiver(this, networkStateFilter);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if(info != null) {
            if(info.isConnected()) {
                DisplayPlaces.PlaceholderFragment.updatePlacesData();
            }
        }
    }
}